﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour {

	[SerializeField] private GameObject[] spawnAreas;
	[SerializeField] private GameObject[] itemsToSpawn;
	[SerializeField] private float spawnTimerMin = 5.0f;
	[SerializeField] private float spawnTimerMax = 10.0f;
	[SerializeField] private int amountToSpawn = 5;
	[SerializeField] private int amountToRespawn = 1;
	[SerializeField] private int spawnCap = 10;
	private int lastIndex = -1;
	private int amountInWorld = 0;
	private float selectedSpawnTimer = 0.0f;
	private float timer = 0.0f;

	// Use this for initialization
	void Start () {
		for ( int i = 0; i < spawnAreas.Length; i++ )
		{
			spawnAreas[i].gameObject.SetActive( false );
		}
	}

	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if ( timer > selectedSpawnTimer && 
			 amountInWorld <= amountToRespawn )
		{
			// Grab a random spawn Area
			int randomSpawnArea = Random.Range( 0, spawnAreas.Length );
			if ( spawnAreas.Length > 1 )
			{
				while ( randomSpawnArea == lastIndex )
				{
					randomSpawnArea = Random.Range( 0, spawnAreas.Length );
				}
				lastIndex = randomSpawnArea;
			}
			BoxCollider spawnBox = spawnAreas[randomSpawnArea].GetComponent<BoxCollider>();
			
			for( int i = 0; i < amountToSpawn; i++ )
			{
				if ( amountInWorld > spawnCap )
				{
					return;
				}
				Vector3 randomPosition = spawnBox.transform.position + spawnBox.center + new Vector3( ( Random.value - 0.5f ) * spawnBox.size.x * transform.localScale.x, ( Random.value - 0.5f ) * spawnBox.size.y * transform.localScale.y, ( Random.value - 0.5f ) * spawnBox.size.z * transform.localScale.z );
				GameObject itemSpawned = GameObject.Instantiate( itemsToSpawn[Random.Range( 0, itemsToSpawn.Length )], randomPosition, Random.rotation );
				itemSpawned.transform.SetParent( transform, true );
				amountInWorld++;
			}

			timer = 0.0f;
			selectedSpawnTimer = Random.Range( spawnTimerMin, spawnTimerMax );
		}
		
	}

	public void DecrementAmount()
	{
		amountInWorld--;
	}
}
