﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour {

	PlayerSpawner playerSpawner;
	ItemSpawner shellSpawner;
	ItemSpawner foodSpawner;


	void Start () {
		playerSpawner = GameObject.Find( "PlayerSpawner" ).GetComponent<PlayerSpawner>();
		foodSpawner = GameObject.Find( "FoodSpawner" ).GetComponent<ItemSpawner>();
		shellSpawner = GameObject.Find( "ShellSpawner" ).GetComponent<ItemSpawner>();
		GetComponent<MeshRenderer>().enabled = false;
	}
	

	void Update () {
		
	}


	private void OnCollisionEnter( Collision collision )
	{
		Debug.Assert( playerSpawner );

		if ( collision.gameObject.CompareTag( "Food" ) )
		{
			foodSpawner.DecrementAmount();
			Destroy( collision.gameObject );
		}
		else if ( collision.gameObject.CompareTag("Shell") )
		{
			shellSpawner.DecrementAmount();
			Destroy( collision.gameObject );
		}
		else if ( collision.gameObject.CompareTag( "Player" ) )
		{
			playerSpawner.RespawnPlayer( collision.gameObject );
		}
	}
}
