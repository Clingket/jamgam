﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RoundManager : MonoBehaviour {

	private float timer = 0.0f;
	[SerializeField]
	private float roundTimer = 0.0f;
	[SerializeField]
	private bool disableTimer = false;
	[SerializeField]
	private Slider slider;
	[SerializeField]
	private Image image;
	[SerializeField]
	private float winningSize = 0.0f;

	private List<Crab> crabs = new List<Crab>();
	[SerializeField] Transform[] podiums;
	[SerializeField] GameObject endOfGame;
	[SerializeField] GameObject timerObject;
	private bool triggered = false;

	[SerializeField]
	private AudioClip audioClip;


	void Start () {	
		Time.timeScale = 1.0f;
		triggered = false;
	}

	// Update is called once per frame
	void Update () {
		if ( roundTimer < Mathf.Epsilon || 
			 disableTimer ||
			 crabs.Count <= 0 ||
			 triggered )
		{
			return;
		}

		bool crabReachedMaxSize = false;
		float largestSize = -1.0f;
		int winningCrab = -1;
		for ( int i = 0; i < crabs.Count; i++ )
		{
			float crabSize = crabs[i].GetSize();
			crabReachedMaxSize = crabSize >= winningSize && winningSize > 0.0f;
			winningCrab = crabSize > largestSize ? i : winningCrab;
			largestSize = Mathf.Max( crabSize, largestSize );
		}

		if ( !crabReachedMaxSize)
		{
			for ( int i = 0; i < crabs.Count; i++ )
			{
				GameObject crabShell = crabs[i].GetShell();
				if ( crabShell )
				{
					crabReachedMaxSize = crabShell.GetComponent<Shell>().IsWinningShell();
					winningCrab = crabReachedMaxSize ? i : winningCrab;
					float crabSize = crabs[i].GetSize();
					largestSize = Mathf.Max( crabSize, largestSize );
				}
			}
		}

		timer = Mathf.Clamp( timer + Time.deltaTime, 0.0f, roundTimer );
		if ( timer >= roundTimer  ||
			 crabReachedMaxSize )
		{
			Debug.Log( "Winner is: " + winningCrab );
			Debug.Assert( winningCrab != -1 );
			// Do winner stuff here.

			for ( int i = 0; i < crabs.Count; i++ )
			{
				crabs[i].enabled = false;
				crabs[i].cameraTransform.gameObject.SetActive( false );
				crabs[i].mainMenu.SetActive( false );
				//GameObject.Find( "Camera " + i ).SetActive( false );
				Debug.Assert( podiums[i] );
				//crabs[i].transform.SetPositionAndRotation( podiums[i].position, podiums[i].rotation );
			}
			Time.timeScale = 1.0f;
			endOfGame.SetActive( true );
			timerObject.SetActive( false );

			crabs[winningCrab].transform.SetPositionAndRotation( podiums[0].position, podiums[0].rotation );
			crabs.Remove( crabs[winningCrab] );

			int podiumIndex = 1;
			while ( crabs.Count > 0 )
			{
				int largestCrabID = -1;
				float largestCrab = 0.0f;
				for ( int i = 0; i < crabs.Count; i++ )
				{
					float crabSize = crabs[i].GetSize();
					largestCrabID = crabSize > largestCrab ? i : largestCrabID;
					largestCrab = Mathf.Max( crabSize, largestCrab );
				}
				crabs[largestCrabID].transform.transform.position = podiums[podiumIndex].position;// SetPositionAndRotation( podiums[podiumIndex].position, podiums[podiumIndex].rotation );
				crabs[largestCrabID].transform.transform.rotation = podiums[podiumIndex].rotation;// SetPositionAndRotation( podiums[podiumIndex].position, podiums[podiumIndex].rotation );
				crabs.Remove( crabs[largestCrabID] );
				podiumIndex++;
			}

			triggered = true;
			StartCoroutine( ExitGame() );
			//Scene scene = SceneManager.GetActiveScene();
			//SceneManager.LoadScene( scene.buildIndex );
		}

		Debug.Assert( slider );
		slider.value = Mathf.Clamp01( timer / roundTimer );
		Debug.Assert( image );
		image.fillAmount = Mathf.Clamp01( timer / roundTimer );
	}

	IEnumerator ExitGame()
	{
		GameObject.Find( "BackgroundMusic" ).GetComponent<AudioSource>().clip = audioClip;
		GameObject.Find( "BackgroundMusic" ).GetComponent<AudioSource>().Play();
		float timer = 0.0f;
		while ( timer < 10.0f )
		{
			timer += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}

		SceneManager.LoadScene( "Menu", LoadSceneMode.Single );
		Time.timeScale = 1.0f;
		yield return null;
	}

	public void GiveCrabScript(Crab crabScript )
	{
		crabs.Add( crabScript );
	}


	public void NukeCrabScript(Crab crabScript )
	{
		crabs.Remove( crabScript );
	}
}
