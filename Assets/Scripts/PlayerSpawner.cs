﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
	[SerializeField]
	private GameObject playerObject;

	[SerializeField]
	private Transform[] spawnTransforms;

	[SerializeField]
	private bool spawnOnlyOnePlayer = false;

	[SerializeField]
	private Texture[] textures;

	private GameObject roundManager;

	int crabsToSpawn;

	void Start()
	{
		Debug.Assert( playerObject );
		Debug.Assert( spawnTransforms.Length == 4 );

		crabsToSpawn = spawnOnlyOnePlayer ? 1 : Input.GetJoystickNames().Length;
		roundManager = GameObject.Find( "RoundManager" );
		Debug.Assert( roundManager );

		for ( int i = 0; i < Input.GetJoystickNames().Length; i++ )
		{
			Debug.Log( "Player: " + i + " is using controller: " + Input.GetJoystickNames()[i] );
		}

		for ( int i = 0; i < crabsToSpawn; i++ )
		{
			Transform spawnTransform = spawnTransforms[i];

			GameObject player = GameObject.Instantiate( playerObject, spawnTransforms[i].position, spawnTransforms[i].rotation );
			GameObject hermitCrabPrefab = player.transform.Find( "HermitCrabPrefab" ).gameObject;
			hermitCrabPrefab.GetComponent<Renderer>().material.mainTexture = textures[i];
			Renderer[] childRenderers = hermitCrabPrefab.GetComponentsInChildren<Renderer>();
			for ( int j = 0; j < childRenderers.Length; j++ )
			{
				childRenderers[j].material.mainTexture = textures[i];
			}

			player.transform.SetParent( transform, true );

			Crab crabScript = player.GetComponent<Crab>();
			Debug.Assert( crabScript );
			crabScript.SetID( i );

			Camera playerCamera = player.GetComponentInChildren<Camera>();
			if ( i > 0 )
			{
				playerCamera.gameObject.GetComponent<AudioListener>().enabled = false;
			}

			if ( spawnOnlyOnePlayer )
			{
				playerCamera.rect = new Rect( 0.0f, 0.0f, 1.0f, 1.0f );
			}
			else
			{
				float rectX;
				float rectY;
				if ( crabsToSpawn  <= 2 )
				{
					rectX = 0;
					rectY = i % 2 == 0 ? 0.0f : 0.5f;
				}
				else
				{
					rectX = i < 2 ? 0.0f : 0.5f;
					rectY = i % 2 == 0 ? 0.0f : 0.5f;
				}
				float width = 0.5f;
				if ( crabsToSpawn <= 2 || ( crabsToSpawn == 3 && i == 1 ) )
				{
					width = 1.0f;
				}
				playerCamera.rect = new Rect( rectX, rectY, width, 0.5f );
			}

			spawnTransform.gameObject.SetActive( false );
			roundManager.GetComponent<RoundManager>().GiveCrabScript( crabScript );
		}
	}


	public void RespawnPlayer( GameObject playerToRespawn )
	{
		int playerID = playerToRespawn.GetComponent<Crab>().GetID();
		roundManager.GetComponent<RoundManager>().NukeCrabScript( playerToRespawn.GetComponent<Crab>() );

		Transform spawnTransform = spawnTransforms[playerID];

		GameObject player = GameObject.Instantiate( playerObject, spawnTransforms[playerID].position, spawnTransforms[playerID].rotation );
		player.transform.SetParent( transform, true );

		Crab crabScript = player.GetComponent<Crab>();
		Debug.Assert( crabScript );
		crabScript.SetID( playerID );

		Camera playerCamera = player.GetComponentInChildren<Camera>();
		if ( playerID > 0 )
		{
			playerCamera.gameObject.GetComponent<AudioListener>().enabled = false;
		}

		if ( spawnOnlyOnePlayer )
		{
			playerCamera.rect = new Rect( 0.0f, 0.0f, 1.0f, 1.0f );
		}
		else
		{
			float rectX;
			float rectY;
			if ( crabsToSpawn <= 2 )
			{
				rectX = 0;
				rectY = playerID % 2 == 0 ? 0.0f : 0.5f;
			}
			else
			{
				rectX = playerID < 2 ? 0.0f : 0.5f;
				rectY = playerID % 2 == 0 ? 0.0f : 0.5f;
			}
			float width = 0.5f;
			if ( crabsToSpawn <= 2 || ( crabsToSpawn == 3 && playerID == 1 ) )
			{
				width = 1.0f;
			}
			playerCamera.rect = new Rect( rectX, rectY, width, 0.5f );
		}

		spawnTransform.gameObject.SetActive( false );
		roundManager.GetComponent<RoundManager>().GiveCrabScript( crabScript );

		GameObject.Destroy( playerToRespawn );
		/*
		Transform spawnTransform = spawnTransforms[playerID];
		playerToRespawn.transform.SetPositionAndRotation( spawnTransform.position, spawnTransform.rotation );
		*/
	}


	void Update()
	{

	}
}
