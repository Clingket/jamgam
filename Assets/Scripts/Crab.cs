﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crab : MonoBehaviour
{
	// Movement
	private Rigidbody rigid;
	private CharacterController characterController;
	private Vector3 desiredMove;
	private Quaternion characterTargetRot;
	private Vector3 impact = Vector3.zero;
	[SerializeField] private float noShellSpeedMultiplier = 1.5f;
	[SerializeField] private float maxSpeed = 0.3f;
	[SerializeField] private float bumpForce = 1.0f;
	[SerializeField] private float playerBumpForce = 1.0f;

	// Growth / Shell
	[SerializeField] private float foodWorth = 0.25f;
	private float size = 0.0f;
	[SerializeField] private float desiredSize = 0.0f;
	[SerializeField] private float bounceMultiplier = 1.2f;
	private GameObject shell = null;
	private float shellDroppedAt = 0.0f;
	[SerializeField] private float ejectMultiplier = 3.0f;
	[SerializeField] private float shellLerpOnDuration = 0.25f;
	private float currentShellMinSize = 1.0f;
	private float currentShellMaxSize = 100.0f;
	private float shellY = 0.01098558f;
	private float shellZ = -0.009072195f;

	// Out of shell death
	[SerializeField] private float deathDurationOutsideShell = 5.0f;
	private float deathTimer = 0.0f;
	private bool playedCatMeowShort = false;
	private TextMesh textMesh;
	private bool dead = false;
	[SerializeField] private GameObject catMesh;
	[SerializeField] private GameObject eatParticles;
	[SerializeField] private GameObject collisionParticles;

	// Controls
	private int ID = -1;
	public Transform cameraTransform;
	Transform cameraTarget;
	[SerializeField] private float maxVerticalAngle = 60.0f;
	[SerializeField] private float minVerticalAngle = 0.0f;
	[SerializeField] private float verticalSensitivity = 1.0f;
	[SerializeField] private float horizontalSensitivity = 3.0f;
	private bool isHoldingGrab = false;


	// dodgy ref tracking food, gross.
	ItemSpawner foodSpawner;
	PlayerSpawner playerSpawner;
	GameObject shellSpawner;
	public GameObject mainMenu;
	Animator animator;


	// Audio
	[SerializeField] private AudioClip scuttleSound; // todo
	[SerializeField] private AudioClip shellPopOffSound;
	[SerializeField] private AudioClip shellPopOnSound;
	[SerializeField] private AudioClip crabImpactSound;
	[SerializeField] private AudioClip eatSound;
	[SerializeField] private AudioClip growSound;
	[SerializeField] private AudioClip catMeowShort;
	[SerializeField] private AudioClip catMeowLong;
	[SerializeField] private AudioClip catSwipe;
	[SerializeField] private AudioClip catHit;
	private AudioSource[] audioSources;
	private AudioSource scuttleSource;
	private AudioSource oneShotSource;


	void Start()
	{
		mainMenu = GameObject.Find( "MainMenuObject" );
		mainMenu = mainMenu.transform.Find( "MainMenu" ).gameObject;
		mainMenu.SetActive( false );

		audioSources = GetComponents<AudioSource>();
		scuttleSource = audioSources[0];
		Debug.Assert( scuttleSource.clip != null );
		oneShotSource = audioSources[1];

		size = transform.localScale.x;
		desiredSize = size;

		rigid = GetComponent<Rigidbody>();
		characterController = GetComponent<CharacterController>();
		animator = transform.Find( "HermitCrabPrefab" ).GetComponent<Animator>();

		characterTargetRot = transform.localRotation;

		cameraTransform = GetComponentInChildren<Camera>().transform;
		cameraTarget = transform.Find( "CameraTarget" );
		cameraTransform.SetParent( null, true );
		cameraTransform.gameObject.name = "Camera " + ID;

		shell = transform.Find( "HermitCrabPrefab" ).Find( "Shell" ).gameObject;
		PickupShell( shell );

		foodSpawner = GameObject.Find( "FoodSpawner" ).GetComponent<ItemSpawner>();
		playerSpawner = GameObject.Find( "PlayerSpawner" ).GetComponent<PlayerSpawner>();
		shellSpawner = GameObject.Find( "ShellSpawner" );

		textMesh = transform.Find( "ImpendingDeath" ).GetComponent<TextMesh>();
		textMesh.text = "";

		deathTimer = 0.0f;
		dead = false;
		playedCatMeowShort = false;
	}


	void Update()
	{
		if ( dead )
		{
			return;
		}

		if ( transform.localScale.x != size )
		{
			transform.localScale = new Vector3( size, size, size );
		}
		if ( shell != null && 
			 shell.transform.localScale.x != 1.0f / size )
		{
			shell.transform.localScale = new Vector3( 1.0f / size, 1.0f / size, 1.0f / size );
		}

		Debug.Assert( ID != -1 );
		desiredMove = transform.forward * -Input.GetAxis( "Vertical" + ID ) + transform.right * Input.GetAxis( "Horizontal" + ID );
		if ( desiredMove.magnitude > maxSpeed )
		{
			desiredMove.Normalize();
			desiredMove *= maxSpeed;
		}

		animator.speed = 1.0f;
		if ( shell == null )
		{
			desiredMove *= noShellSpeedMultiplier;
			animator.speed = noShellSpeedMultiplier;
			deathTimer += Time.deltaTime;
		}
		else
		{
			deathTimer = Mathf.Max( deathTimer - Time.deltaTime, 0.0f );
			if ( deathTimer < deathDurationOutsideShell / 2 && playedCatMeowShort )
			{
				playedCatMeowShort = false;
			}
		}

		isHoldingGrab = Input.GetButton( "Grab" + ID );
		
		if ( Input.GetButtonDown( "Drop" + ID ) )
		{
			shellDroppedAt = Time.time;
			DropShell();
		}

		Vector3 toMove = Vector3.ProjectOnPlane( desiredMove, Vector3.up );
		toMove *= Time.deltaTime;
		toMove.Set( toMove.x, -1.0f, toMove.z );
		toMove += ( impact * Time.deltaTime );
		characterController.Move( toMove );
		if ( desiredMove.magnitude > 0.001f && !scuttleSource.isPlaying )
		{
			scuttleSource.Play();
		}
		else if ( desiredMove.magnitude <= 0.001f && scuttleSource.isPlaying )
		{
			scuttleSource.Stop();
		}

		// consumes the impact energy each cycle:
		impact = Vector3.Lerp( impact, Vector3.zero, 5 * Time.deltaTime );

		if ( deathTimer > deathDurationOutsideShell )
		{
			dead = true;
			StartCoroutine( KillPlayer() );
		}
		else if ( deathTimer > 0.0f )
		{
			if ( deathTimer > deathDurationOutsideShell / 2  && !playedCatMeowShort )
			{
				oneShotSource.PlayOneShot( catMeowShort );
				playedCatMeowShort = true;
			}
			textMesh.text = "!";
			textMesh.fontSize = (int)Mathf.Lerp( 1, 60, deathTimer / deathDurationOutsideShell );
		}

		if ( Input.GetButtonDown( "Start" + ID ) )
		{
			mainMenu.SetActive( true );
			Time.timeScale = 0.0f;
		}
	}


	IEnumerator KillPlayer()
	{
		catMesh.SetActive( true );
		float timer = 0.0f;
		oneShotSource.PlayOneShot( catSwipe );
		float initalHeight = catMesh.transform.position.y;
		while ( timer < 0.25f )
		{
			float yValue = Mathf.Lerp( initalHeight, 0.045f, timer / 0.3f );
			catMesh.transform.position = new Vector3( catMesh.transform.position.x, yValue, catMesh.transform.position.z );
			timer += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}

		oneShotSource.PlayOneShot( catHit );
		catMesh.transform.position = new Vector3( catMesh.transform.position.x, 0.045f, catMesh.transform.position.z );
		oneShotSource.PlayOneShot( catHit );
		GameObject.Destroy( cameraTransform.gameObject );
		playerSpawner.RespawnPlayer( gameObject );
		yield return null;
	}


	private void LateUpdate()
	{
		cameraTransform.position = Vector3.Lerp( cameraTransform.position, cameraTarget.position, Time.deltaTime * 10.0f );
		cameraTransform.rotation = Quaternion.Slerp( cameraTransform.rotation, cameraTarget.rotation, Time.deltaTime * 10.0f );

		float yRot = Input.GetAxis( "LookHorizontal" + ID ) * horizontalSensitivity;
		if ( Mathf.Abs(yRot) > 0.01f )
		{
			characterTargetRot *= Quaternion.Euler( 0.0f, yRot, 0.0f );
		}
		transform.localRotation = characterTargetRot;

		float xRot = Input.GetAxis( "LookVertical" + ID ) * verticalSensitivity;
		if ( Mathf.Abs(xRot) > 0.01f )
		{
			cameraTarget.RotateAround( transform.position, transform.right, xRot );
			if ( cameraTarget.localEulerAngles.x > maxVerticalAngle ||
				 cameraTarget.localEulerAngles.x < minVerticalAngle )
			{
				cameraTarget.RotateAround( transform.position, transform.right, -xRot );
			}
		}
	}


	private void FixedUpdate()
	{
		//if ( rigid.velocity.sqrMagnitude < desiredMove.sqrMagnitude &&
			 //desiredMove.magnitude > 0.00001f )
		{
			Debug.DrawLine( transform.position, transform.position + Vector3.ProjectOnPlane( desiredMove, Vector3.up ) * 2.0f, Color.red, 0.0f );
			//transform.Translate( Vector3.ProjectOnPlane( desiredMove, Vector3.up ), Space.World );
			//rigid.MovePosition( transform.position + Vector3.ProjectOnPlane( desiredMove, Vector3.up ) );
			//rigid.AddForce( Vector3.ProjectOnPlane( desiredMove, Vector3.up ), ForceMode.Impulse );
		}
	}


	public void AddImpact( Vector3 dir, float force )
	{
		dir.Normalize();
		if ( dir.y < 0 ) dir.y = -dir.y; // reflect down force on the ground
		impact += dir.normalized * force / /*mass*/1.0f;
	}


	void OnControllerColliderHit( ControllerColliderHit hit )
	{
		Rigidbody body = hit.collider.attachedRigidbody;

		if ( hit.gameObject.CompareTag("Player") )
		{
			float otherCrabSize = hit.gameObject.GetComponent<Crab>().GetSize();
			float biggestCrab = Mathf.Max( otherCrabSize, size );
			float sizeModifier = Mathf.Lerp( 1.0f, 5.0f, Mathf.Clamp01( biggestCrab / 10.0f ) );
			AddImpact( ( transform.position - hit.transform.position ), ( otherCrabSize / biggestCrab ) * playerBumpForce );
			hit.gameObject.GetComponent<Crab>().AddImpact( ( hit.transform.position - transform.position ), ( size / biggestCrab ) * playerBumpForce );
			DropShell();
			hit.gameObject.GetComponent<Crab>().DropShell();
			oneShotSource.PlayOneShot( crabImpactSound );
			Instantiate( collisionParticles, hit.transform.position, hit.transform.rotation );
			return;
		}

		else if ( hit.gameObject.CompareTag( "Food" ) )
		{
			desiredSize += foodWorth;
			Instantiate( eatParticles, hit.transform.position, hit.transform.rotation );
			foodSpawner.DecrementAmount();
			Destroy( hit.gameObject );
			if ( desiredSize > currentShellMaxSize )
			{
				DropShell();
			}
			StopCoroutine( SizePop() );
			StartCoroutine( SizePop() );
			oneShotSource.PlayOneShot( eatSound );
			return;
		}

		else if ( isHoldingGrab && 
			 hit.gameObject.CompareTag( "Shell" ) &&
			 Time.time + 0.25 > shellDroppedAt )
		{
			shellDroppedAt = Time.time;
			DropShell();
			PickupShell( hit.gameObject );
			return;
		}

		else if ( hit.gameObject.CompareTag( "Shell" ) )
		{
			Vector3 pushDir = ( hit.transform.position - transform.position );
			float shellMinSize = hit.gameObject.GetComponent<Shell>().GetShellMinSize();
			float sizeDiff = size - shellMinSize;
			hit.gameObject.GetComponent<Rigidbody>().AddForce( pushDir * bumpForce * 100.0f * ( sizeDiff / 2.0f ), ForceMode.Impulse );
			//body.velocity = pushDir * size * bumpForce * 20.0f;
		}
	}


	IEnumerator SizePop()
	{
		float timer = 0;
		float bounceDurationExpand = 0.25f;
		float bounceDurationShrink = 0.1f;
		while ( timer < bounceDurationExpand )
		{
			size = Mathf.Lerp( size, desiredSize * bounceMultiplier, timer / bounceDurationExpand );
			timer += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		timer = 0.0f;
		while ( timer < bounceDurationShrink )
		{
			size = Mathf.Lerp( size, desiredSize, timer / bounceDurationShrink );
			timer += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}

		size = desiredSize;
		oneShotSource.PlayOneShot( growSound );
		yield return null;
	}


	public void SetID( int intendedID )
	{
		ID = intendedID;
	}


	public int GetID()
	{
		return ID;
	}


	public float GetSize()
	{
		return size;
	}


	public GameObject GetShell()
	{
		return shell;
	}


	private void DropShell()
	{
		if ( shell == null )
		{
			return;
		}

		// Check key press first but for now just swap.
		shell.GetComponent<MeshCollider>().enabled = true;
		shell.transform.SetParent( shellSpawner.transform, true );
		shell.GetComponent<Rigidbody>().detectCollisions = true;
		shell.GetComponent<Rigidbody>().isKinematic = false;
		shell.GetComponent<Rigidbody>().AddForce( -transform.forward * ejectMultiplier * size, ForceMode.Impulse );
		shell.layer = LayerMask.NameToLayer( "NoPlayerCollide" );
		StartCoroutine( ReenablePlayerCollision( shell ) );
		shell = null;

		oneShotSource.PlayOneShot( shellPopOffSound );
	}


	IEnumerator ReenablePlayerCollision( GameObject ejectedShell )
	{
		yield return new WaitForSeconds(0.25f);
		ejectedShell.layer = LayerMask.NameToLayer( "Default" );
		yield return null;
	}


	private void PickupShell( GameObject newShell )
	{
		shell = newShell;
		shell.transform.SetParent( transform.Find("HermitCrabPrefab"), true );
		shell.GetComponent<MeshCollider>().enabled = false;
		shell.GetComponent<Rigidbody>().detectCollisions = false;
		shell.GetComponent<Rigidbody>().isKinematic = true;
		shell.layer = LayerMask.NameToLayer( "NoPlayerCollide" );
		currentShellMinSize = shell.GetComponent<Shell>().GetShellMinSize();
		currentShellMaxSize = shell.GetComponent<Shell>().GetShellMaxSize();
		if ( size < currentShellMinSize || size > currentShellMaxSize )
		{
			DropShell();
		}
		else
		{
			StartCoroutine( LerpShellToPlayer() );
		}
	}


	IEnumerator LerpShellToPlayer()
	{
		float timer = 0;
		while ( timer < shellLerpOnDuration )
		{
			if ( shell == null )
			{
				yield return null;
			}
			shell.transform.position = Vector3.Lerp( shell.transform.position, transform.position + new Vector3( 0.0f, shellY, shellZ ), timer / shellLerpOnDuration );
			shell.transform.rotation = Quaternion.Slerp( shell.transform.rotation, transform.rotation, timer / shellLerpOnDuration );
			timer += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		if ( shell == null )
		{
			yield return null;
		}
		shell.transform.localPosition = new Vector3( 0.0f, shellY, shellZ );
		shell.transform.localRotation = Quaternion.identity;
		oneShotSource.PlayOneShot( shellPopOnSound );
		yield return null;
	}
}
