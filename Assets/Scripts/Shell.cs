﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour
{
	[SerializeField] private float minSize;
	[SerializeField] private float maxSize;
	[SerializeField] private bool winningShell = false;


	public float GetShellMinSize()
	{
		return minSize;
	}


	public float GetShellMaxSize()
	{
		return maxSize;
	}


	public bool IsWinningShell()
	{
		return winningShell;
	}
}
