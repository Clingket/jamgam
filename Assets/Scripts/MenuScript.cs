﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {

	int buttonSelected = 0;
	[SerializeField] GameObject[] buttons;
	float[] buttonLastChangedAt = new float[4];
	float buttonChangeDuration = 0.25f;
	[SerializeField] GameObject controls;
	[SerializeField] GameObject credits;
	[SerializeField] bool mainMenu = true;
	AudioSource audioSource;
	[SerializeField] AudioClip pressButtonSound;


	private void Start()
	{
		audioSource = GetComponent<AudioSource>();
	}


	void Update () {
		if ( controls.activeSelf || credits.activeSelf )
		{
			bool pressedBack = false;
			for ( int i = 0; i < Input.GetJoystickNames().Length; i++ )
			{
				pressedBack |= Input.GetButton( "Drop" + i );
			}

			if ( pressedBack )
			{
				audioSource.PlayOneShot( pressButtonSound );
				controls.SetActive( false );
				credits.SetActive( false );
				for ( int i = 0; i < buttons.Length; i++ )
				{
					buttons[i].SetActive( true );
				}
			}
			return;
		}

		for ( int i = 0; i < Input.GetJoystickNames().Length; i++ )
		{
			buttons[buttonSelected].GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
			if ( Mathf.Abs( Input.GetAxis( "Vertical" + i ) ) <= 0.2f )
			{
				buttonLastChangedAt[i] = 0.0f; // Let us change button if we've gone back to deadzone.
			}
			else if ( Input.GetAxis( "Vertical" + i ) > 0.8f )
			{
				if ( Time.time > buttonLastChangedAt[i] + buttonChangeDuration )
				{
					buttonSelected++;
					buttonLastChangedAt[i] = Time.time;
				}
			}
			else if ( Input.GetAxis( "Vertical" + i ) < -0.8f )
			{
				if ( Time.time > buttonLastChangedAt[i] + buttonChangeDuration )
				{
					buttonSelected--;
					buttonLastChangedAt[i] = Time.time;
				}
			}
			buttonSelected = (int)Mathf.Clamp( buttonSelected, 0, buttons.Length - 1 );
		}

		buttons[buttonSelected].GetComponent<RectTransform>().localScale = new Vector3(1.25f, 1.25f, 1.25f);

		bool pressedContinue = false;
		bool pressedStart = false;
		for ( int i = 0; i < Input.GetJoystickNames().Length; i++ )
		{
			pressedContinue |= Input.GetButton( "Grab" + i );
			pressedStart |= Input.GetButtonDown( "Start" + i );
		}

		if ( pressedStart && !mainMenu )
		{
			audioSource.PlayOneShot( pressButtonSound );
			GameObject.Find( "MainMenu" ).SetActive( false );
			Time.timeScale = 1.0f;
		}

		if ( pressedContinue )
		{
			audioSource.PlayOneShot( pressButtonSound );
			if ( buttonSelected == 0 )
			{
				if ( mainMenu  )
				{
					SceneManager.LoadScene( "Environment", LoadSceneMode.Single );
				}
				else
				{
					GameObject.Find( "MainMenu" ).SetActive( false );
					Time.timeScale = 1.0f;
				}
			}
			if ( buttonSelected == 1 )
			{
				for ( int i = 0; i < buttons.Length; i++ )
				{
					buttons[i].SetActive( false );
				}
				controls.SetActive( true );
			}
			if ( buttonSelected == 2 )
			{
				for ( int i = 0; i < buttons.Length; i++ )
				{
					buttons[i].SetActive( false );
				}
				credits.SetActive( true );
				credits.GetComponent<RectTransform>().sizeDelta = new Vector2( Screen.width, Screen.height );
			}
			if ( buttonSelected == 3 )
			{
				if ( mainMenu  )
				{
					Application.Quit();
				}
				else
				{
					SceneManager.LoadScene( "Menu", LoadSceneMode.Single );
					Time.timeScale = 1.0f;
				}
			}
		}
	}
}
